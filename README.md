# Atesh.PostSharp

Atesh.PostSharp library is a collection of custom made PostSharp Aspects (a.k.a Attributes).

# Prerequisites

* **PostSharp:** The purpose of this library is to provide additional features to PostSharp so, to be able to use this library, you should already be using PostSharp in your project. It is a commercial product as a Visual Studio Extension but it offers community license for free.

PostSharp is the #1 pattern-aware extension to C# and VB. It allows developers to eradicate boilerplate by offloading repeating work from humans to machines. Please see [PostSharp Documentation](https://doc.postsharp.net) for more info.

# Team Members

* Project Lead: Onur "Xtro" Er, Atesh Entertainment Inc. onurer@gmail.com

# Download and Install

You can directly install the library via [NuGet](https://www.nuget.org/packages/Atesh.PostSharp).

**OR**

Download it via "manual download" link in [NuGet](https://www.nuget.org/packages/Atesh.PostSharp) web page and extract the assembly into your project manually if you don't want to use a NuGet client.

"nupkg" file you downloaded from NuGet web page is a regular zip file. You can change its extension to "zip" and extract it easily.

# Contribution

You can easily contribute to the project by just reporting issues to [here](https://bitbucket.org/XtroTheArctic/Atesh.PostSharp/issues)

If you want to get involved and actively contribute to the project, you can simply do so by sending pull requests to the project lead via bitbucket.com.

Project page on [Bitbucket](https://bitbucket.org/XtroTheArctic/Atesh.PostSharp)

Git Repo URL: git@bitbucket.org:XtroTheArctic/atesh.postsharp.git

Please feel free to contact the team members via email at any time.

## How To Build

If you want to build the library from source code to contribute to the project or for another reason, you must install the PostSharp Visual Studio extension first. Please see the "Prerequisites" section above and [PostSharp Website](https://postsharp.net) for more info.

After installing the extension, you need to get a PostSharp community license for free. You can request the license in PostSharp license section of Visual Studio options window or on thier website directly.

# The Unlicense

This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or distribute this software, either in source code form or as a compiled binary, for any purpose, commercial or non-commercial, and by any means.

In jurisdictions that recognize copyright laws, the author or authors of this software dedicate any and all copyright interest in the software to the public domain. We make this dedication for the benefit of the public at large and to the detriment of our heirs and successors. We intend this dedication to be an overt act of relinquishment in perpetuity of all present and future rights to this software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org>