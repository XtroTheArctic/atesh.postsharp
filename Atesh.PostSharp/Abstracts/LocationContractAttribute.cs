﻿using System;
using PostSharp.Aspects;
using PostSharp.Extensibility;
using PostSharp.Serialization;

namespace Atesh.PostSharp;

[PSerializable]
[MulticastAttributeUsage]
[AttributeUsage(AttributeTargets.Field | AttributeTargets.Property | AttributeTargets.Parameter | AttributeTargets.ReturnValue)]
public abstract class LocationContractAttribute : LocationLevelAspect
{
    protected LocationContractAttribute() => AspectPriority = 10;

    protected virtual string GetErrorMessage() => null;

    protected Exception CreateArgumentException(string LocationName) => new ArgumentException(GetErrorMessage(), LocationName);
    protected Exception CreateArgumentNullException(string LocationName) => new ArgumentNullException(LocationName);
    protected Exception CreateArgumentOutOfRangeException(string LocationName, object Value) => new ArgumentOutOfRangeException(LocationName, Value, GetErrorMessage());
}