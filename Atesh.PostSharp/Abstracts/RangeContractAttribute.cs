﻿using System;
using PostSharp.Aspects;
using PostSharp.Extensibility;
using PostSharp.Reflection;
using PostSharp.Serialization;

namespace Atesh.PostSharp;

[PSerializable]
public abstract class RangeContractAttribute : LocationContractAttribute, ILocationValidationAspect<int>, ILocationValidationAspect<long>, ILocationValidationAspect<float>, ILocationValidationAspect<double>
{
    // ReSharper disable once FieldCanBeMadeReadOnly.Global
    protected object Value1;

    protected RangeContractAttribute(IComparable Value) => Value1 = Value;

    public override bool CompileTimeValidate(LocationInfo LocationInfo)
    {
        if (Value1.GetType() == LocationInfo.LocationType) return true;

        Message.Write(MessageLocation.Of(LocationInfo), SeverityType.Fatal, "Atesh", Strings.TypeMismatchBetweenLocationAndContract(LocationInfo.Name, GetType().FullName));

        return false;
    }

    protected abstract bool Validate(IComparable Value);

    public Exception ValidateValue(int Value, string LocationName, LocationKind LocationKind, LocationValidationContext Context) => !Validate(Value) ? CreateArgumentOutOfRangeException(LocationName, Value) : null;
    public Exception ValidateValue(long Value, string LocationName, LocationKind LocationKind, LocationValidationContext Context) => !Validate(Value) ? CreateArgumentOutOfRangeException(LocationName, Value) : null;
    public Exception ValidateValue(float Value, string LocationName, LocationKind LocationKind, LocationValidationContext Context) => !Validate(Value) ? CreateArgumentOutOfRangeException(LocationName, Value) : null;
    public Exception ValidateValue(double Value, string LocationName, LocationKind LocationKind, LocationValidationContext Context) => !Validate(Value) ? CreateArgumentOutOfRangeException(LocationName, Value) : null;
}