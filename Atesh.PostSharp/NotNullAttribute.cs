﻿using System;
using PostSharp.Aspects;
using PostSharp.Reflection;
using PostSharp.Serialization;

namespace Atesh.PostSharp;

[PSerializable]
public class NotNullAttribute : LocationContractAttribute, ILocationValidationAspect<object>
{
    public Exception ValidateValue(object Value, string LocationName, LocationKind LocationKind, LocationValidationContext Context) => Value is { } ? null : CreateArgumentNullException(LocationName);
}