﻿using System;
using PostSharp.Serialization;

namespace Atesh.PostSharp;

[PSerializable]
public class GreaterThanOrEqualAttribute : RangeContractAttribute
{
    public GreaterThanOrEqualAttribute(int Value) : base(Value) { }
    public GreaterThanOrEqualAttribute(long Value) : base(Value) { }
    public GreaterThanOrEqualAttribute(float Value) : base(Value) { }
    public GreaterThanOrEqualAttribute(double Value) : base(Value) { }

    protected override string GetErrorMessage() => Strings.ValueCantBeLessThan(Value1.ToString());

    protected override bool Validate(IComparable Value) => Value.CompareTo(Value1) >= 0;
}