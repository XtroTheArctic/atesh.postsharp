﻿using System;
using PostSharp.Serialization;

namespace Atesh.PostSharp;

[PSerializable]
public class GreaterThanAttribute : RangeContractAttribute
{
    public GreaterThanAttribute(int Value) : base(Value) { }
    public GreaterThanAttribute(long Value) : base(Value) { }
    public GreaterThanAttribute(float Value) : base(Value) { }
    public GreaterThanAttribute(double Value) : base(Value) { }

    protected override string GetErrorMessage() => Strings.ValueMustBeGreaterThan(Value1.ToString());

    protected override bool Validate(IComparable Value) => Value.CompareTo(Value1) > 0;
}
