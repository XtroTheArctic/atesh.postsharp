﻿using System;
using PostSharp.Serialization;

namespace Atesh.PostSharp;

[PSerializable]
public class RangeAttribute : RangeContractAttribute
{
    // ReSharper disable once FieldCanBeMadeReadOnly.Global
    protected IComparable Value2;

    public RangeAttribute(int Min, int Max) : base(Min) => Value2 = Max;
    public RangeAttribute(long Min, long Max) : base(Min) => Value2 = Max;
    public RangeAttribute(float Min, float Max) : base(Min) => Value2 = Max;
    public RangeAttribute(double Min, double Max) : base(Min) => Value2 = Max;

    protected override string GetErrorMessage() => Strings.ValueMustBeBetween(Value1.ToString(), Value2.ToString());

    protected override bool Validate(IComparable Value) => Value.CompareTo(Value1) >= 0 && Value.CompareTo(Value1) <= 0;
}