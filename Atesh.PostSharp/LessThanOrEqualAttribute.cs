﻿using System;
using PostSharp.Serialization;

namespace Atesh.PostSharp;

[PSerializable]
[AttributeUsage(AttributeTargets.Parameter)]
public class LessThanOrEqualAttribute : RangeContractAttribute
{
    public LessThanOrEqualAttribute(int Value) : base(Value) { }
    public LessThanOrEqualAttribute(long Value) : base(Value) { }
    public LessThanOrEqualAttribute(float Value) : base(Value) { }
    public LessThanOrEqualAttribute(double Value) : base(Value) { }

    protected override string GetErrorMessage() => Strings.ValueCantBeGreaterThan(Value1.ToString());

    protected override bool Validate(IComparable Value) => Value.CompareTo(Value1) <= 0;
}