﻿using System;
using PostSharp.Serialization;

namespace Atesh.PostSharp;

[PSerializable]
public class LessThanAttribute : RangeContractAttribute
{
    public LessThanAttribute(int Value) : base(Value) { }
    public LessThanAttribute(long Value) : base(Value) { }
    public LessThanAttribute(float Value) : base(Value) { }
    public LessThanAttribute(double Value) : base(Value) { }

    protected override string GetErrorMessage() => Strings.ValueMustBeLessThan(Value1.ToString());

    protected override bool Validate(IComparable Value) => Value.CompareTo(Value1) < 0;
}