﻿using System;
using System.Collections.Generic;
using System.Reflection;
using PostSharp.Aspects;
using PostSharp.Aspects.Advices;
using PostSharp.Serialization;

namespace Atesh.PostSharp;

[PSerializable]
public class ValidateOperationAttribute : OnMethodBoundaryAspect, IInstanceScopedAspect, IAdviceProvider
{
    // ImportMethodAdviceInstance class needs this field as public.
    // ReSharper disable once UnassignedField.Global
    public Action Validate;

    // ReSharper disable once FieldCanBeMadeReadOnly.Local
    string MethodName;

    public ValidateOperationAttribute(string MethodName)
    {
        this.MethodName = MethodName;

        AspectPriority = 100;
    }

    public object CreateInstance(AdviceArgs AdviceArgs) => MemberwiseClone();

    public void RuntimeInitializeInstance() { }

    public IEnumerable<AdviceInstance> ProvideAdvices(object TargetElement)
    {
        yield return new ImportMethodAdviceInstance(GetType().GetField(nameof(Validate), BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static), MethodName, true);
    }

    // Sealed for PostSharp performance warning.
    public sealed override void OnEntry(MethodExecutionArgs Args) => Validate();
}